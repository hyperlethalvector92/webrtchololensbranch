﻿using System;
using System.Collections;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.WebRTC;
using UnityEngine;
using NativeWebSocket;
namespace DenkiKitchen.WebRTC{
    public class WebSocketSignaler : Microsoft.MixedReality.WebRTC.Unity.Signaler
    {
        public bool mustCreateOffer = true;
        public bool isServer = false;
        // Start is called before the first frame update
        public string Address;

        WebSocket websocket;
        public ConnectionId roomID;
        public int myID;
        public string myIDStr;
        public string otherIDStr;
        bool isFirstIDMessage = false;
        bool hasInitialized = false;
        public void CreateOfferHost(){
            if(socketIsActive){
                isServer = true;                
                if(mustCreateOffer){
                    PeerConnection.StartConnection();
                }
            }
        }
        public void CreateInitialContact(){
            NetworkEvent ne = new NetworkEvent(NetEventType.ServerInitialized,new ConnectionId(-1),"qwer");
            websocket.Send(NetworkEvent.ToByteArray(ne));
        }
        public void CreateOfferClient(){
            if(socketIsActive){
                isServer = false;                
            }
        }
        public void ReloadScene(){
            UnityEngine.SceneManagement.Scene scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene(); 
            UnityEngine.SceneManagement.SceneManager.LoadScene(scene.name);
        }
        public void ReceiveMessageData(byte[] b){
            Debug.Log(b.Length);
            System.Text.UnicodeEncoding ue2 = new System.Text.UnicodeEncoding();            
            string message = System.Text.Encoding.UTF8.GetString(b); 
            Debug.Log("Received data: " + message);
            knownDataChannels[0].SendMessage(System.Text.Encoding.UTF32.GetBytes("Acknowledged"));
        }
        public void ReceivedMessageUnsafe(IntPtr ptr, ulong length){
            Debug.Log("received unsafe message of length: " + length);
        }
        public List<DataChannel> knownDataChannels = new List<DataChannel>();
        public void DataChannelAddedDelegate(DataChannel channel){
            Debug.Log("Data Channel Added, ID: " + channel.ID + ", Label: " + channel.Label);
            channel.MessageReceived += ReceiveMessageData;
            channel.MessageReceivedUnsafe += ReceivedMessageUnsafe;
            knownDataChannels.Add(channel);
        }

        public override void OnPeerInitialized(){
            base.OnPeerInitialized();
            PeerConnection.Peer.VideoTrackAdded += VideoTrackAdded;
            PeerConnection.Peer.DataChannelAdded += DataChannelAddedDelegate;
            AfterInit2();
            
        }
        public void VideoTrackAdded(Microsoft.MixedReality.WebRTC.RemoteVideoTrack track){
            Debug.Log("Added video track library: " + track.ToString());
        }
        public void OnMessageReceived(byte[] b){

        }
        public bool socketIsActive = false;
        async void AfterInit2()
        {
            websocket = new WebSocket(Address);
            websocket.OnOpen += () =>
            {
                Debug.Log("Socket is open");
                socketIsActive = true;
            };

            websocket.OnError += (e) =>
            {
                Debug.Log("Error! " + e);
            };

            websocket.OnClose += (e) =>
            {
                Debug.Log("Connection closed!");
                socketIsActive = false;
            };

            websocket.OnMessage += (bytes) =>
            {
            // Reading a plain text message
//                var message = System.Text.Encoding.UTF8.GetString(bytes);

                ProcessMessage(bytes);
            };
            // Keep sending messages at every 0.3s
            await websocket.Connect();

        }
        public void ProcessMessage(byte[] bytes){
            NetworkEvent ne = NetworkEvent.FromByteArray(bytes);
            switch(ne.Type){
                case NetEventType.NewConnection:                      
                    roomID = ne.ConnectionId;
                break;                      
                case NetEventType.ReliableMessageReceived: 
                    System.Text.UnicodeEncoding ue2 = new System.Text.UnicodeEncoding();            
                    string message = ue2.GetString(ne.GetDataAsByteArray());        
                    if(message.Contains("sdp")){
                        SdpMessageWebsocket sdpmws = JsonUtility.FromJson<SdpMessageWebsocket>(message);
                        string ss = JsonUtility.ToJson(sdpmws); 
                        if (string.Equals(sdpmws.type, "offer", StringComparison.OrdinalIgnoreCase)){
                            Debug.Log("Received Offer: " + sdpmws.sdp);
                            var sdpOffer = new SdpMessage { Type = SdpMessageType.Offer, Content = sdpmws.sdp};
                            PeerConnection.HandleConnectionMessageAsync(sdpOffer).ContinueWith(_ =>
                            {
                                // If the remote description was successfully applied then immediately send
                                // back an answer to the remote peer to acccept the offer.
                                _nativePeer.CreateAnswer();
                            }, TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.RunContinuationsAsynchronously);                          

                        }else if (string.Equals(sdpmws.type, "answer", StringComparison.OrdinalIgnoreCase)){
                            Debug.Log("Received Answer: " + sdpmws.sdp);
                            var sdpAnswer = new SdpMessage { Type = SdpMessageType.Answer, Content = sdpmws.sdp};                            
                            PeerConnection.HandleConnectionMessageAsync(sdpAnswer).ContinueWith(_ =>
                            {
                                Debug.Log("Handled Answer");
                            }, TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.RunContinuationsAsynchronously);
                        }else{
                            IceMessageWebsocket imws = JsonUtility.FromJson<IceMessageWebsocket>(message);
                            if(imws.candidate != String.Empty){
                                _nativePeer.AddIceCandidate(new IceCandidate{SdpMid = imws.sdpMid,SdpMlineIndex = imws.sdpMlineIndex,Content = imws.candidate});
                            }else{
                                Debug.Log("Message Unknown: " + message);
                            }
                        }
                    }else if(message.Contains("candidate")){    
                        Debug.Log("Trying to parse an ICE message");
                        IceMessageWebsocket imws = JsonUtility.FromJson<IceMessageWebsocket>(message);
                        if(imws.candidate != String.Empty){
                            _nativePeer.AddIceCandidate(new IceCandidate{SdpMid = imws.sdpMid,SdpMlineIndex = imws.sdpMlineIndex,Content = imws.candidate});
                        }else{
                            Debug.Log("Message Unknown: " + message);
                        }
                    }else{
                        Debug.Log("Wasn't Ice Message");
                        otherIDStr = message;                               
                        if(isServer){
                            myIDStr = "9999999999";
                            System.Text.UnicodeEncoding ue = new System.Text.UnicodeEncoding();
                            ByteArrayBuffer bab = new ByteArrayBuffer(ue.GetBytes(myIDStr));
                            NetworkEvent nn = new NetworkEvent(NetEventType.ReliableMessageReceived,roomID, bab);                                               
                            NEStack.Add(nn);

                            System.Threading.Thread.Sleep(100);
                            _nativePeer.CreateOffer();                                    
                        }else{
                            myIDStr = "0000000001";                                                                                                        
                            System.Text.UnicodeEncoding ue = new System.Text.UnicodeEncoding();
                            ByteArrayBuffer bab = new ByteArrayBuffer(ue.GetBytes(myIDStr));
                            NetworkEvent nn = new NetworkEvent(NetEventType.ReliableMessageReceived,roomID, bab);                                               
                            NEStack.Add(nn);                                    
                        }
                    }
                break;
            }
        }
        List<NetworkEvent> NEStack =  new List<NetworkEvent>();
        public async void SendMessage(){

        }
        public async void OnDestroy(){
            NetworkEvent ne = new NetworkEvent(NetEventType.ServerClosed,new ConnectionId(-1),"qwer");
            await websocket.Send(NetworkEvent.ToByteArray(ne));
            websocket.Close();
        }
        protected override void Update(){
            base.Update();
            Update2();
        }
        // Update is called once per frame
        async void Update2()
        {
            if(NEStack.Count > 0){
                NetworkEvent ne = NEStack[0];
                NEStack.RemoveAt(0);
                byte[] b = NetworkEvent.ToByteArray(ne);
                await websocket.Send(b);
            }
        }
        byte[] getArrayFromInt(int intValue){
            byte[] intBytes = BitConverter.GetBytes(intValue);
            Array.Reverse(intBytes);
            return intBytes;
        }
        int getIntFromArray(byte[] value){
            return BitConverter.ToInt32(value, 0);
        }
        #region ISignaler interface

        /// <inheritdoc/>
        public override Task SendMessageAsync(SdpMessage message)
        {
            return SendMessageImplAsync();
        }

        /// <inheritdoc/>
        public override Task SendMessageAsync(IceCandidate candidate)
        {
            return SendMessageImplAsync();
        }

        #endregion

        private Task SendMessageImplAsync()
        {
            // This method needs to return a Task object which gets completed once the signaler message
            // has been sent. Because the implementation uses a Unity coroutine, use a reset event to
            // signal the task to complete from the coroutine after the message is sent.
            // Note that the coroutine is a Unity object so needs to be started from the main Unity app thread.
            // Also note that TaskCompletionSource<bool> is used as a no-result variant; there is no meaning
            // to the bool value.
            // https://stackoverflow.com/questions/11969208/non-generic-taskcompletionsource-or-alternative
//            var tcs = new TaskCompletionSource<bool>();
  //          _mainThreadWorkQueue.Enqueue(() => StartCoroutine(PostToServerAndWait(message, tcs)));
            return null;
        }
        protected override void OnIceCandidateReadyToSend(IceCandidate candidate)
        {
          //Data = string.Join(IceSeparatorChar, candidate.Content, candidate.SdpMlineIndex.ToString(), candidate.SdpMid);      
            IceMessageWebsocket imws = new IceMessageWebsocket(candidate.Content,candidate.SdpMlineIndex,candidate.SdpMid);
            string json = JsonUtility.ToJson(imws);  
            System.Text.UnicodeEncoding ue = new System.Text.UnicodeEncoding();
            ByteArrayBuffer bab = new ByteArrayBuffer(ue.GetBytes(json));              
            NetworkEvent ne = new NetworkEvent(NetEventType.ReliableMessageReceived,roomID,bab);
            NEStack.Add(ne);
        }
        protected override void OnSdpOfferReadyToSend(SdpMessage offer)
        {

            SdpMessageWebsocket sdpws = new SdpMessageWebsocket(offer.Content,"offer");
    //        sdpws.sdp = sdpws.sdp.Replace("msid:- ","msid:");
            Debug.Log("Sending Offer: " + sdpws.sdp);                        
            string json = JsonUtility.ToJson(sdpws);
            System.Text.UnicodeEncoding ue = new System.Text.UnicodeEncoding();
            ByteArrayBuffer bab = new ByteArrayBuffer(ue.GetBytes(json));              
            NetworkEvent ne = new NetworkEvent(NetEventType.ReliableMessageReceived,roomID,bab);
            NEStack.Add(ne);
        }
        protected override void OnSdpAnswerReadyToSend(SdpMessage answer)
        {

            SdpMessageWebsocket sdpws = new SdpMessageWebsocket(answer.Content,"answer");
//            sdpws.sdp = sdpws.sdp.Replace("msid:- ","msid:");            
  //          sdpws.sdp = sdpws.sdp.Replace("a=msid-semantic: WMS","a=msid-semantic: WMS stream_label");
            Debug.Log("Sending Answer: " + sdpws.sdp);
            string json = JsonUtility.ToJson(sdpws);
            System.Text.UnicodeEncoding ue = new System.Text.UnicodeEncoding();
            ByteArrayBuffer bab = new ByteArrayBuffer(ue.GetBytes(json));              
            NetworkEvent ne = new NetworkEvent(NetEventType.ReliableMessageReceived,roomID,bab);
            NEStack.Add(ne);
        }
    }




    [System.Serializable]
    public class IceMessageWebsocket{
        public string candidate;
        public int sdpMlineIndex;
        public string sdpMid;
        public IceMessageWebsocket(string cand, int sdpmline, string sdpmid){
            candidate = cand;
            sdpMlineIndex = sdpmline;
            sdpMid = sdpmid;
        }
    }
    [System.Serializable]
    public class SdpMessageWebsocket{
        public string sdp;
        public string type;
        public SdpMessageWebsocket(string sdpMessage, string sdpType){
            sdp = sdpMessage;
            type = sdpType; 
        }
    }
    [System.Serializable]
    public struct ConnectionId
    {
        public static readonly ConnectionId INVALID = new ConnectionId()
        {
        id = -1
        };
        public short id;

        public ConnectionId(short lId)
        {
        this.id = lId;
        }

        public override bool Equals(object obj)
        {
        return obj is ConnectionId connectionId && connectionId == this;
        }

        public bool IsValid()
        {
        return this != ConnectionId.INVALID;
        }

        public override int GetHashCode()
        {
        return this.id.GetHashCode();
        }

        public static bool operator ==(ConnectionId i1, ConnectionId i2)
        {
        return (int) i1.id == (int) i2.id;
        }

        public static bool operator !=(ConnectionId i1, ConnectionId i2)
        {
        return !(i1 == i2);
        }

        public static bool operator <(ConnectionId i1, ConnectionId i2)
        {
        return (int) i1.id < (int) i2.id;
        }

        public static bool operator >(ConnectionId i1, ConnectionId i2)
        {
        return (int) i1.id > (int) i2.id;
        }

        public static bool operator <=(ConnectionId i1, ConnectionId i2)
        {
        return (int) i1.id <= (int) i2.id;
        }

        public static bool operator >=(ConnectionId i1, ConnectionId i2)
        {
        return (int) i1.id >= (int) i2.id;
        }

        public override string ToString()
        {
        return this.id.ToString();
        }
    }
    #region re-implementation of the webrtc socket library
    public enum NetEventType : byte
    {
        /// <summary>
        /// Should never happen. Indicades an error / failed to initialize properly
        /// </summary>
        Invalid = 0,
        /// <summary>
        /// Used for UDP style messages
        /// </summary>
        UnreliableMessageReceived = 1,
        /// <summary>
        /// Used for TCP style messages
        /// </summary>
        ReliableMessageReceived = 2,
        /// <summary>
        /// confirmation that the server was started. other people will be able to connect
        /// </summary>
        ServerInitialized = 3,
        /// <summary>
        /// server couldn't be started
        /// </summary>
        ServerInitFailed = 4,
        /// <summary>
        /// server was closed. no new incoming connections
        /// </summary>
        ServerClosed = 5,
        /// <summary>
        /// 
        /// new incoming or outgoing connection established
        /// </summary>
        NewConnection = 6,
        /// <summary>
        /// outgoing connection failed
        /// </summary>
        ConnectionFailed = 7,
        /// <summary>
        /// a connection was disconnected
        /// </summary>
        Disconnected = 8,
        /// <summary>
        /// not yet used. Is suppose to be used for errors
        /// that indicate the system that returned it stopped working.
        /// (e.g. buffer overflow)
        /// </summary>
        FatalError = 100,
        /// <summary>
        /// not yet used
        /// </summary>
        Warning = 101,
        /// <summary>
        ///not yet used
        /// </summary>
        Log = 102,

        /// <summary>
        /// This value and higher are reserved for other uses. 
        /// Should never get to the user and should be filtered out.
        /// </summary>
        ReservedStart = 200,
        /// <summary>
        /// Reserved.
        /// Used by protocols that forward NetworkEvents
        /// </summary>
        MetaVersion = 201,
        /// <summary>
        /// Reserved.
        /// Used by protocols that forward NetworkEvents.
        /// </summary>
        MetaHeartbeat = 202
    }

    /// <summary>
    /// Byte in the raw messages that indicate the content. 
    /// This will be removed again in the future after the native side properly supports encoded strings
    /// 
    /// </summary>
        [System.Serializable]
    public enum NetEventDataType : byte
    {
        /// <summary>
        /// No data / invalid
        /// </summary>
        Null = 0,
        /// <summary>
        /// leading 32 bit byte length + byte array
        /// </summary>
        ByteArray = 1,
        /// <summary>
        /// leading 32 bit length (in utf16 chunks)  + UTF 16 
        /// </summary>
        UTF16String = 2,
    }
    public interface MessageDataBuffer : System.IDisposable
    {
        byte[] Buffer { get; }

        int Offset { get; }

        int ContentLength { get; }
    }
    /// <summary>
    /// Contains information about events received by the network.
    /// 
    /// The type of the network event decides the content it can contain.
    /// 
    /// Most important are:
    /// 
    /// UnreliableMessageReceived / ReliableMessageReceived:
    /// A new message was received. The property MessageData will return
    /// a buffer + byte array containing the data received.
    /// 
    /// ServerInitialized:
    /// A call to StartServer was successful. The Info property will return the address
    /// the server can be accessed by.
    /// 
    /// 
    /// </summary>
    public class ErrorInfo
    {
        public static readonly string SERVER_INIT_FAILED_ADDRESS_IN_USE = "Failed to reserve address. Address is already in use";
        public static readonly string CONNECTION_FAILED_ADDRESS_UNKNOWN = "Failed to connect to the given address. Address is not in use.";
        public static readonly string SERVER_INIT_FAILED_REQURED_CONNECTION_OFFLINE = "Failed to reserve address. Required network connection is offline.";
        public static readonly string CONNECTION_FAILED_REQURED_CONNECTION_OFFLINE = "Failed to connect. Required network connection is offline.";
        public static readonly string DISCONNECTED_REQURED_CONNECTION_OFFLINE = "Disconnected. Required network connection is offline.";
        public static readonly string SERVER_CLOSED_REQURED_CONNECTION_OFFLINE = "Server closed unexpectedly. No new connections can be accepted. Required network connection is offline.";
        public static readonly string CONNECTION_FAILED_TO_CONNECT_DIRECTLY = "Failed to connect. Failed to establish a direct connection to the other side. A firewall might block the connection.";
        public static readonly string INCOMING_CONNECTION_FAILED_TO_CONNECT_DIRECTLY = "An incomming connection attempt failed. ";
        public static readonly string DISCONNECTED_DUE_TO_TIMEOUT = "Disconnected due to timeout";
        public static readonly string CONFIGURATION_FAILED = "Media configuration failed. Failed to access requested media.";
        private string mErrorMessage;

        public string ErrorMessage
        {
        get
        {
            return this.mErrorMessage;
        }
        }

        public ErrorInfo(string error)
        {
        this.mErrorMessage = error;
        }

        public override string ToString()
        {
        return this.mErrorMessage;
        }
    }
    [System.Serializable]
    public struct NetworkEvent
{
    private NetEventType type;

    /// <summary>
    /// Returns the type of the message.
    /// </summary>
    public NetEventType Type
    {
        get
        {
            return type;
        }
    }

    private ConnectionId connectionId;

    /// <summary>
    /// Returns the related connection id or ConnecitonId.Invalid if there is none.
    /// </summary>
    public ConnectionId ConnectionId
    {
        get
        {
            return connectionId;
        }
    }


    private object data;

    /// <summary>
    /// Returns an object belonging to the event.
    /// This can be a MessageDataBuffer containing a byte array or a string or an errorinfo object
    /// </summary>
    public object RawData
    {
        get { return data; }
    }
    /// <summary>
    /// Returns the content of the messages if the event type is
    /// UnreliableMessageReceived or ReliableMessageReceived.
    /// 
    /// null for all other message types.
    /// </summary>
    public MessageDataBuffer MessageData
    {
        get
        {
            return data as MessageDataBuffer;
        }
    }

    /// <summary>
    /// Returns the a copy of the message data as a new byte array with the exact size of the content
    /// instead of the buffer.
    /// </summary>
    /// <returns>Copy of the message data or null if no message data available</returns>
    public byte[] GetDataAsByteArray()
    {
        if(MessageData != null)
        {
            byte[] arr = new byte[MessageData.ContentLength];
            Array.Copy(MessageData.Buffer, MessageData.Offset, arr, 0, MessageData.ContentLength);
            return arr;
        }
        return null;
    }
    
    /// <summary>
    /// Contains additional information or null
    /// Only used so far for NetEventType.ServerInitialized to return the servers address information.
    /// </summary>
    public string Info
    {
        get
        {
            if(data is string)
            {
                return (string)data;
            }
            else if(data != null)
            {
                return data.ToString();
            }else{
                return null;
            }
        }
    }
    /// <summary>
    /// Returns an error info object if one is connected. 
    /// </summary>
    public ErrorInfo ErrorInfo
    {
        get
        {
            return data as ErrorInfo;
        }
    }

    /// <summary>
    /// Creates a new network event of a certain type setting 
    /// connection id to invalid and data to null.
    /// 
    /// Internal only. Do not use.
    /// </summary>
    /// <param name="t">The type of this event</param>
    public NetworkEvent(NetEventType t)
    {
        type = t;
        connectionId = ConnectionId.INVALID;
        data = null;
    }

    /// <summary>
    /// Events without data connected
    /// </summary>
    /// <param name="t"></param>
    /// <param name="conId"></param>
    public NetworkEvent(NetEventType t, ConnectionId conId)
    {
        type = t;
        connectionId = conId;
        data = null;
    }

    /// <summary>
    /// Creates a network event with the given content
    /// Internal only. Do not use.
    /// </summary>
    /// <param name="t">Type name</param>
    /// <param name="conId">ConnectionId the event is from / relates to</param>
    /// <param name="dt">MessageDataBuffer (should be ByteArrayBuffer)</param>
    public NetworkEvent(NetEventType t, ConnectionId conId, MessageDataBuffer dt)
    {
        type = t;
        connectionId = conId;
        data = dt;
        if(data != null && (data is ByteArrayBuffer) == false && (data is string) == false)
        {
            throw new ArgumentException("data can only be ByteArrayBuffer or string");
        }
    }
    public NetworkEvent(NetEventType t, ConnectionId conId, object dt)
    {
        type = t;
        connectionId = conId;
        data = dt;
        if (data != null && (data is ByteArrayBuffer) == false && (data is string) == false)
        {
            throw new ArgumentException("data can only be ByteArrayBuffer or string");
        }
    }
    /// <summary>
    /// ServerInit events with address info
    /// </summary>
    /// <param name="t"></param>
    /// <param name="conId"></param>
    /// <param name="address"></param>
    public NetworkEvent(NetEventType t, ConnectionId conId, string address)
    {
        type = t;
        connectionId = conId;
        data = address;
    }

    /// <summary>
    /// Network event with ErrorInfo attached.
    /// ErrorInfo can't be serialized yet!
    /// </summary>
    /// <param name="t"></param>
    /// <param name="conId"></param>
    /// <param name="errorInfo"></param>
    public NetworkEvent(NetEventType t, ConnectionId conId, ErrorInfo errorInfo)
    {
        type = t;
        connectionId = conId;
        data = errorInfo;
    }

    /// <summary>
    /// Converts the event to string. Use for debugging only.
    /// </summary>
    /// <returns>A string representation of the network event.</returns>
    public override string ToString()
    {
        System.Text.StringBuilder datastring = new System.Text.StringBuilder();
        datastring.Append("NetworkEvent type: ");
        datastring.Append(type);
        datastring.Append(" connection: ");
        datastring.Append(connectionId);
        datastring.Append(" data: ");

        if (data is ByteArrayBuffer)
        {
            ByteArrayBuffer msg = (ByteArrayBuffer)data;

            //datastring.Append(Encoding.ASCII.GetString(msg.array, msg.offset, msg.positionWrite));
            datastring.Append(BitConverter.ToString(msg.array, msg.Offset, msg.PositionWriteAbsolute));

        }
        else
        {
            datastring.Append(data);
        }
        return datastring.ToString();
    }


    /// <summary>
    /// Checks if this is a special event that isn't suppose to be
    /// processed as NetworkEvent.
    /// Special events are null, empty byte[0] and any values 
    /// starting with a byte equal or above NetEventType.ReservedStart;
    /// 
    /// 
    /// </summary>
    /// <param name="arr"></param>
    /// <returns>
    /// true - means the message needs special handling and can't be
    /// passed to FromByteArray.
    /// 
    /// false - means the message can be a NetworkEvent but
    /// could still be invalid.
    /// </returns>
    public static bool IsMetaEvent(byte[] arr)
    {
        byte reservedStart = (byte)NetEventType.ReservedStart;
        if(arr == null || arr.Length == 0 || arr[0] >= reservedStart)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Reverse of ToByteArray.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static NetworkEvent FromByteArray(byte[] arr)
    {
        NetEventType type = (NetEventType)arr[0];
        NetEventDataType dataType = (NetEventDataType)arr[1];
        ConnectionId conId = new ConnectionId(BitConverter.ToInt16(arr, 2));

        if(dataType == NetEventDataType.ByteArray)
        {
            uint length = BitConverter.ToUInt32(arr, 4);
            ByteArrayBuffer buff = ByteArrayBuffer.Get((int)length);
            for(uint i = 0; i < length; i++)
            {
                buff.Buffer[buff.Offset + i] = arr[8 + i];
            }
            buff.PositionWriteRelative = (int)length;
            return new NetworkEvent(type, conId, buff);
        }
        else if (dataType == NetEventDataType.UTF16String)
        {
            uint length = BitConverter.ToUInt32(arr, 4);
            length = length * 2; //to bytes
            string text = System.Text.Encoding.Unicode.GetString(arr, 8, (int)length);
            return new NetworkEvent(type, conId, text);
        }
        else
        {
            return new NetworkEvent(type, conId);
        }

    }

    /// <summary>
    /// Mainly used by WebsocketNetwork. It serializes network events into a byte array to allow
    /// to process the events on a different system than where it actually occurred.
    /// 
    /// e.g. WebsocketNetwork uses a server that will receive incoming connects and then
    /// serializes that event into a byte array and send it to the client which holds the
    /// address of the incoming connection to process the event
    /// 
    /// NOTE: This method is not optimized and shouldn't be used with a lot of data!
    /// </summary>
    /// <param name="evt">Network event to be serialized</param>
    /// <returns>Newly created byte array representing the network event</returns>
    public static byte[] ToByteArray(NetworkEvent evt)
    {
        //TODO: check utf 16 standard and if we can use the UTF16 version of C# with the encoding in
        //java script which just moves the values of the characters into an byte array
        //use the full 16 bit values until java script version gets a proper utf 8 or 16 encoding
        bool falseUtf16 = true;
        uint length = 4;
        byte dataType = (byte)NetEventDataType.Null;
        if (evt.data is ByteArrayBuffer)
        {
            dataType = (byte)NetEventDataType.ByteArray;
            length += (uint)(4 + evt.MessageData.ContentLength);
        }
        else if(evt.data is string)
        {
            dataType = (byte)NetEventDataType.UTF16String;
            string str = evt.data as string;
            if(falseUtf16)
            {
                length += (uint)(4 + str.Length * 2);
            }
            else
            {
                length += (uint)(4 + System.Text.Encoding.Unicode.GetByteCount(str));
            }
        }

        byte[] arr = new byte[length];
        arr[0] = (byte)evt.type;
        arr[1] = dataType;
        byte[] id = BitConverter.GetBytes(evt.connectionId.id);
        arr[2] = id[0];
        arr[3] = id[1];
        if (evt.data is ByteArrayBuffer)
        {
            byte[] dataLengthBytes = BitConverter.GetBytes(evt.MessageData.ContentLength);
            arr[4] = dataLengthBytes[0];
            arr[5] = dataLengthBytes[1];
            arr[6] = dataLengthBytes[2];
            arr[7] = dataLengthBytes[3];

            int dataOffset = evt.MessageData.Offset;
            for (int i = 0; i < evt.MessageData.ContentLength; i++)
            {
                arr[8 + i] = evt.MessageData.Buffer[dataOffset + i];
            }
        }
        else if (evt.data is string)
        {

            if (falseUtf16)
            {
                string str = evt.data as string;

                byte[] dataLengthBytes = BitConverter.GetBytes(str.Length);
                arr[4] = dataLengthBytes[0];
                arr[5] = dataLengthBytes[1];
                arr[6] = dataLengthBytes[2];
                arr[7] = dataLengthBytes[3];
                for(int i = 0; i < str.Length; i++)
                {
                    ushort val = str[i];
                    arr[8 + i * 2] = (byte)val;
                    arr[8 + i * 2 + 1] = (byte)(val >> 8);
                }
            }
            else
            {
                string str = evt.data as string;

                byte[] dataLengthBytes = BitConverter.GetBytes(System.Text.Encoding.Unicode.GetByteCount(str) / 2);
                arr[4] = dataLengthBytes[0];
                arr[5] = dataLengthBytes[1];
                arr[6] = dataLengthBytes[2];
                arr[7] = dataLengthBytes[3];
                System.Text.Encoding.Unicode.GetBytes(str, 0, str.Length, arr, 8);
            }
        }
        return arr;
    }

    /// <summary>
    /// Internal use only. Used to attach additional error information.
    /// </summary>
    /// <param name="error"></param>
    public void AttachError(ErrorInfo error)
    {
        this.data = error;
    }
}
    [System.Serializable]
    public class ByteArrayBuffer : MessageDataBuffer, IDisposable
    {
        private static List<ByteArrayBuffer>[] sPool = new List<ByteArrayBuffer>[32];
        public static bool LOG_GC_CALLS = false;
        private static int[] MultiplyDeBruijnBitPosition = new int[32]
        {
        0,
        1,
        28,
        2,
        29,
        14,
        24,
        3,
        30,
        22,
        20,
        15,
        25,
        17,
        4,
        8,
        31,
        27,
        13,
        23,
        21,
        19,
        16,
        7,
        26,
        12,
        18,
        6,
        11,
        5,
        10,
        9
        };
        private bool mFromPool = true;
        public byte[] array;
        private int positionWrite;
        private int positionRead;
        private int offset;
        private bool mDisposed;

        public int PositionWriteRelative
        {
        get
        {
            return this.positionWrite;
        }
        set
        {
            this.positionWrite = value;
        }
        }

        public int PositionWriteAbsolute
        {
        get
        {
            return this.positionWrite + this.offset;
        }
        set
        {
            this.positionWrite = value - this.offset;
        }
        }

        public int PositionReadRelative
        {
        get
        {
            return this.positionRead;
        }
        }

        public int PositionReadAbsolute
        {
        get
        {
            return this.positionRead + this.offset;
        }
        }

        public int Offset
        {
        get
        {
            return this.offset;
        }
        }

        public byte[] Buffer
        {
        get
        {
            if (this.mDisposed)
            throw new InvalidOperationException("Object is already disposed. No further use allowed.");
            return this.array;
        }
        }

        public int ContentLength
        {
        get
        {
            if (this.mDisposed)
            throw new InvalidOperationException("Object is already disposed. No further use allowed.");
            return this.positionWrite;
        }
        set
        {
            this.positionWrite = value;
        }
        }

        public bool IsDisposed
        {
        get
        {
            return this.mDisposed;
        }
        }

        private ByteArrayBuffer(int size)
        {
        this.mFromPool = true;
        this.array = new byte[size];
        this.offset = 0;
        this.positionWrite = 0;
        this.positionRead = 0;
        }

        public ByteArrayBuffer(byte[] arr)
        : this(arr, 0, arr.Length)
        {
        }

        public ByteArrayBuffer(byte[] arr, int offset, int length)
        {
        this.mFromPool = false;
        this.array = arr;
        this.offset = offset;
        this.positionRead = 0;
        this.positionWrite = length;
        }

        private void Reset()
        {
        this.mDisposed = false;
        this.positionRead = 0;
        this.positionWrite = 0;
        }

        ~ByteArrayBuffer()
        {
        if (this.mDisposed || !this.mFromPool || !ByteArrayBuffer.LOG_GC_CALLS)
            return;
        //SLog.LW((object) "ByteArrayBuffer wasn't disposed.");
        }

        public void CopyFrom(byte[] arr, int srcOffset, int len)
        {
        System.Buffer.BlockCopy((Array) arr, srcOffset, (Array) this.array, this.offset, len);
        this.positionWrite = len;
        }

        static ByteArrayBuffer()
        {
        lock (ByteArrayBuffer.sPool)
        {
            for (int index = 0; index < ByteArrayBuffer.sPool.Length; ++index)
            ByteArrayBuffer.sPool[index] = new List<ByteArrayBuffer>();
        }
        }

        private static int GetPower(uint anyPowerOfTwo)
        {
        uint num = anyPowerOfTwo * 125613361U >> 27;
        return ByteArrayBuffer.MultiplyDeBruijnBitPosition[(int) num];
        }

        private static uint NextPowerOfTwo(uint v)
        {
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        ++v;
        return v;
        }

        public static ByteArrayBuffer Get(int size, bool enforceZeroOffset = false)
        {
        uint anyPowerOfTwo = ByteArrayBuffer.NextPowerOfTwo((uint) size);
        if (anyPowerOfTwo < 128U)
            anyPowerOfTwo = 128U;
        int power = ByteArrayBuffer.GetPower(anyPowerOfTwo);
        ByteArrayBuffer byteArrayBuffer;
        lock (ByteArrayBuffer.sPool)
        {
            if (ByteArrayBuffer.sPool[power].Count == 0)
            {
            byteArrayBuffer = new ByteArrayBuffer((int) anyPowerOfTwo);
            }
            else
            {
            List<ByteArrayBuffer> byteArrayBufferList = ByteArrayBuffer.sPool[power];
            byteArrayBuffer = byteArrayBufferList[byteArrayBufferList.Count - 1];
            byteArrayBufferList.RemoveAt(byteArrayBufferList.Count - 1);
            byteArrayBuffer.Reset();
            }
        }
        return byteArrayBuffer;
        }

        public void Dispose()
        {
        lock (ByteArrayBuffer.sPool)
        {
            if (this.mDisposed)
            throw new InvalidOperationException("Object is already disposed. No further use allowed.");
            this.mDisposed = true;
            if (!this.mFromPool)
            return;
            int power = ByteArrayBuffer.GetPower((uint) this.array.Length);
            ByteArrayBuffer.sPool[power].Add(this);
        }
        }
    }
    #endregion
}